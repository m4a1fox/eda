# Eda application.

After pull run next command for install dependencies

    $ npm install

After all dependencies installed:

    $ DEBUG=folder ./bin/www

Create migrations:

    $ db-migrate create migration_name

To run migrations:

    $ db-migrate up

To rollback migrations:

    $ db-migrate down

