/**
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

/**
 * @author Maxim Bogdanov <sin666m4a1fox@gmail.com>
 */

var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('eda_dev.db');

module.exports = {
    getAllMembers: function (cb) {
        db.serialize(function () {
            var data = [];
            db.each("SELECT * FROM members", function (err, row) {
                data.push(row);
            }, function () {
                cb(data);
            });
        });
    },

    addNewMember: function (data, cb) {
        var stmt = db.prepare("INSERT INTO members VALUES (?, ?, ?, ?)");
        stmt.run(null, data.name, data.surname, data.second_name);
        stmt.finalize();
        cb();
    },

    deleteMemberById: function (id, cb) {
        db.run("DELETE FROM members WHERE id = ?", {1: id});
        cb();
    }
}