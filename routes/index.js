var express = require('express');
var router = express.Router();
var member = require('../models/member');

/* GET home page. */
router.get('/', function(req, res) {
    member.getAllMembers(function(data) {
        res.render('index', { title: 'Express', data: data });
    })

});

module.exports = router;
