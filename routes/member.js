var express = require('express');
var router = express.Router();
var member = require('../models/member');

exports.add_member = function(req, res) {
    if(req.method == 'GET') {
        res.render('member', { title: 'Express Member Add New' });
    } else {
        member.addNewMember(req.body, function() {
            res.redirect('/');
        });
    }
};

exports.delete_member = function(req, res) {
    if(req.method == 'GET') {
        var id = req.param('id');

        member.deleteMemberById(id, function() {
            res.redirect('/');
        });
    } else {
        res.redirect('/');
    }
};


