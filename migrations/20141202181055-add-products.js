var dbm = require('db-migrate');
var type = dbm.dataType;

exports.up = function(db, callback) {
    db.createTable('products', {
        id: { type: 'int', primaryKey: true },
        name: 'string',
        price: 'real'
    }, function() {
        db.insert('products', ['name', 'price'], ['Кефир', 24.45], callback);
    });
};

exports.down = function(db, callback) {
    db.dropTable('product', function(err) {}, callback);
};
