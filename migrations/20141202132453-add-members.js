var dbm = require('db-migrate');
var type = dbm.dataType;

exports.up = function(db, callback) {
    db.createTable('members', {
        id: { type: 'int', primaryKey: true },
        name: 'string',
        surname: 'string',
        second_name: 'string'
    }, function() {
        db.insert('members', ['name', 'surname', 'second_name'], ['Максим', 'Богданов', 'Максимович'], callback);
    });
};

exports.down = function(db, callback) {
    db.dropTable('members', function(err) {}, callback);
};
